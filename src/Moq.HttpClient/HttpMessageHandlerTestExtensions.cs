﻿using System;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using Moq.Language.Flow;
using Moq.Protected;

namespace Qurrent.Net.Http.TestExtensions
{
	/// <summary>
	/// Helper class providing methods for mocking HttpMessageHandlers with the Moq framework.
	/// </summary>
	public static class HttpMessageHandlerTestExtensions
	{
		/// <summary>
		/// Sets up the mock to return an OkNegotiatedContentResponse.
		/// </summary>
		/// <param name="handlerMock">The handler mock to use.</param>
		/// <param name="expectedRequestUri">The expected request URI that is expected to be called. If uri is null, any uri will satisfy the condition (ItExpr.IsAny()).</param>
		/// <param name="content">The content to return within the response.</param>
		public static IReturnsResult<HttpMessageHandler> SetUpOkContentResponse(this Mock<HttpMessageHandler> handlerMock, Uri expectedRequestUri, HttpContent content)
		{
			var response = new HttpResponseMessage(HttpStatusCode.OK)
			{
				Content = content
			};
			return SetUpResponse(handlerMock, expectedRequestUri, response);
		}

		/// <summary>
		/// Sets up the mock to return an OkNegotiatedContentResponse.
		/// </summary>
		/// <param name="handlerMock">The handler mock to use.</param>
		/// <param name="expectedRequestUri">The expected request URI that is expected to be called. If uri is null, any uri will satisfy the condition (ItExpr.IsAny()).</param>
		/// <param name="httpMethod">The HttpMethod the setup should evaluate against.</param>
		/// <param name="content">The content to return within the response.</param>
		public static IReturnsResult<HttpMessageHandler> SetUpOkContentResponse(this Mock<HttpMessageHandler> handlerMock, Uri expectedRequestUri, HttpMethod httpMethod, HttpContent content)
		{
			var response = new HttpResponseMessage(HttpStatusCode.OK)
			{
				Content = content
			};
			return CustomSetUpResponse(handlerMock, req => req.RequestUri == expectedRequestUri && req.Method == httpMethod, response);
		}


		/// <summary>
		/// Sets up the mock to return an OkNegotiatedContentResponse.
		/// </summary>
		/// <param name="handlerMock">The handler mock to use.</param>
		/// <param name="requestConditions">The predicate to evaluate the request message against.</param>
		/// <param name="content">The content to return within the response.</param>
		public static IReturnsResult<HttpMessageHandler> CustomSetUpOkContentResponse(this Mock<HttpMessageHandler> handlerMock, Expression<Func<HttpRequestMessage, bool>> requestConditions, HttpContent content)
		{
			var response = new HttpResponseMessage(HttpStatusCode.OK)
			{
				Content = content
			};
			return CustomSetUpResponse(handlerMock, requestConditions, response);
		}

		/// <summary>
		/// Sets up the mock to return a custom response.
		/// </summary>
		/// <param name="handlerMock">The handler mock to use.</param>
		/// <param name="expectedRequestUri">The expected request URI that is expected to be called. If uri is null, any uri will satisfy the condition (ItExpr.IsAny()).</param>
		/// <param name="responseToReturn">The response to return if the URI is called by the mock.</param>
		public static IReturnsResult<HttpMessageHandler> SetUpResponse(this Mock<HttpMessageHandler> handlerMock, Uri expectedRequestUri, HttpResponseMessage responseToReturn)
		{
			return handlerMock.CustomSetUpResponse(req => req.RequestUri == expectedRequestUri, responseToReturn);
		}

		/// <summary>
		/// Sets up the mock to return a custom response.
		/// </summary>
		/// <param name="handlerMock">The handler mock to use.</param>
		/// <param name="expectedRequestUri">The expected request URI that is expected to be called. If uri is null, any uri will satisfy the condition (ItExpr.IsAny()).</param>
		/// <param name="httpMethod">The HttpMethod the setup should evaluate against.</param>
		/// <param name="responseToReturn">The response to return if the URI is called by the mock.</param>
		public static IReturnsResult<HttpMessageHandler> SetUpResponse(this Mock<HttpMessageHandler> handlerMock, Uri expectedRequestUri, HttpMethod httpMethod, HttpResponseMessage responseToReturn)
		{
			return handlerMock.CustomSetUpResponse(req => req.RequestUri == expectedRequestUri && req.Method == httpMethod, responseToReturn);
		}

		/// <summary>
		/// Sets up response and makes the setup verifiable.
		/// </summary>
		/// <param name="handlerMock">The handler mock to use.</param>
		/// <param name="requestConditions">The predicate to evaluate the request message against.</param>
		/// <param name="responseToReturn">The response to return if the URI is called by the mock.</param>
		public static IReturnsResult<HttpMessageHandler> CustomSetUpResponse(this Mock<HttpMessageHandler> handlerMock, Expression<Func<HttpRequestMessage, bool>> requestConditions, HttpResponseMessage responseToReturn)
		{
			return handlerMock
				.SetupSendAsync(requestConditions)
				.ReturnsAsync(responseToReturn);
		}

		/// <summary>
		/// Sets up the mock to throw a specific exception upon calling and makes the setup verifiable.
		/// </summary>
		/// <param name="handlerMock">The handler mock.</param>
		/// <param name="expectedRequestUri">The expected request URI that is expected to be called. If uri is null, any uri will satisfy the condition (ItExpr.IsAny()).</param>
		/// <param name="exception">The exception to throw.</param>
		public static IReturnsResult<HttpMessageHandler> SetUpThrowsException(this Mock<HttpMessageHandler> handlerMock, Uri expectedRequestUri, Exception exception)
		{
			return handlerMock.CustomSetUpThrowsException(req => req.RequestUri == expectedRequestUri, exception);
		}

		/// <summary>
		/// Sets up the mock to throw a specific exception upon calling and makes the setup verifiable.
		/// </summary>
		/// <param name="handlerMock">The handler mock.</param>
		/// <param name="expectedRequestUri">The expected request URI that is expected to be called. If uri is null, any uri will satisfy the condition (ItExpr.IsAny()).</param>
		/// <param name="httpMethod">The HttpMethod the setup should evaluate against.</param>
		/// <param name="exception">The exception to throw.</param>
		public static IReturnsResult<HttpMessageHandler> SetUpThrowsException(this Mock<HttpMessageHandler> handlerMock, Uri expectedRequestUri, HttpMethod httpMethod, Exception exception)
		{
			return handlerMock.CustomSetUpThrowsException(req => req.RequestUri == expectedRequestUri && req.Method == httpMethod, exception);
		}

		/// <summary>
		/// Sets up the mock to throw a specific exception upon calling and makes the setup verifiable.
		/// </summary>
		/// <param name="handlerMock">The handler mock.</param>
		/// <param name="requestConditions">The predicate to evaluate the request message against.</param>
		/// <param name="exception">The exception to throw.</param>
		public static IReturnsResult<HttpMessageHandler> CustomSetUpThrowsException(this Mock<HttpMessageHandler> handlerMock, Expression<Func<HttpRequestMessage, bool>> requestConditions, Exception exception)
		{
			return handlerMock
				.SetupSendAsync(requestConditions)
				.ThrowsAsync(exception);
		}

		/// <summary>
		/// Sets up the mock to throw an exception upon calling and makes the setup verifiable.
		/// </summary>
		/// <typeparam name="TException">The type of the exception.</typeparam>
		/// <param name="handlerMock">The handler mock.</param>
		/// <param name="expectedRequestUri">The expected request URI that is expected to be called.</param>
		public static IThrowsResult SetUpThrowsException<TException>(this Mock<HttpMessageHandler> handlerMock, Uri expectedRequestUri) where TException : Exception, new()
		{
			return CustomSetUpThrowsException<TException>(handlerMock, req => req.RequestUri == expectedRequestUri);
		}

		/// <summary>
		/// Sets up the mock to throw an exception upon calling and makes the setup verifiable.
		/// </summary>
		/// <typeparam name="TException">The type of the exception.</typeparam>
		/// <param name="handlerMock">The handler mock.</param>
		/// <param name="expectedRequestUri">The expected request URI that is expected to be called.</param>
		/// <param name="httpMethod">The HTTP method to evaluate against.</param>
		public static IThrowsResult SetUpThrowsException<TException>(this Mock<HttpMessageHandler> handlerMock, Uri expectedRequestUri, HttpMethod httpMethod) where TException : Exception, new()
		{
			return CustomSetUpThrowsException<TException>(handlerMock, req => req.RequestUri == expectedRequestUri && req.Method == httpMethod);
		}

		/// <summary>
		/// Sets up the mock to throw an exception upon calling and makes the setup verifiable.
		/// </summary>
		/// <typeparam name="TException">The type of the exception.</typeparam>
		/// <param name="handlerMock">The handler mock.</param>
		/// <param name="requestConditions">The predicate to evaluate the request message against.</param>
		public static IThrowsResult CustomSetUpThrowsException<TException>(this Mock<HttpMessageHandler> handlerMock, Expression<Func<HttpRequestMessage, bool>> requestConditions) where TException : Exception, new()
		{
			return handlerMock
				.SetupSendAsync(requestConditions)
				.Throws<TException>();
		}

		/// <summary>
		/// Sets up the SendAsync method.
		/// </summary>
		/// <param name="handlerMock">The handler mock.</param>
		/// <param name="uri">The uri the handler is expected to call.</param>
		public static ISetup<HttpMessageHandler, Task<HttpResponseMessage>> SetupSendAsync(this Mock<HttpMessageHandler> handlerMock, Uri uri)
		{
			return handlerMock.SetupSendAsync(req => req.RequestUri == uri);
		}

		/// <summary>
		/// Sets up the SendAsync method.
		/// </summary>
		/// <param name="handlerMock">The handler mock.</param>
		/// <param name="uri">The uri the handler is expected to call.</param>
		/// <param name="httpMethod">The HttpMethod the setup should evaluate against.</param>
		public static ISetup<HttpMessageHandler, Task<HttpResponseMessage>> SetupSendAsync(this Mock<HttpMessageHandler> handlerMock, Uri uri, HttpMethod httpMethod)
		{
			return handlerMock.SetupSendAsync(req => req.RequestUri == uri && req.Method == httpMethod);
		}		
		
		/// <summary>
		/// Sets up the SendAsync method.
		/// </summary>
		/// <param name="handlerMock">The handler mock.</param>
		/// <param name="requestConditions">The predicate to evaluate the request message against.</param>
		public static ISetup<HttpMessageHandler, Task<HttpResponseMessage>> SetupSendAsync(this Mock<HttpMessageHandler> handlerMock, Expression<Func<HttpRequestMessage, bool>> requestConditions)
		{
			var requestExpr = EvaluatePredicateAndReturnExpression(requestConditions);
			return handlerMock
				.Protected()
				.Setup<Task<HttpResponseMessage>>("SendAsync", requestExpr, ItExpr.IsAny<CancellationToken>());
		}

		private static Expression EvaluatePredicateAndReturnExpression(Expression<Func<HttpRequestMessage, bool>> requestConditions)
		{
			return requestConditions != null
				? ItExpr.Is(requestConditions)
				: ItExpr.IsAny<HttpRequestMessage>();
		}

		/// <summary>
		/// Verifies the request was sent to the specified uri.
		/// </summary>
		/// <param name="handlerMock">The handler mock.</param>
		/// <param name="uri">The uri to evaluate against.</param>
		/// <param name="times">The times the statement should have executed.</param>
		public static void VerifyRequestWasSent(this Mock<HttpMessageHandler> handlerMock, Uri uri, Times times)
		{
			handlerMock.VerifyRequestWasSent(req => req.RequestUri == uri, times);
		}

		/// <summary>
		/// Verifies the request was sent to the specified uri.
		/// </summary>
		/// <param name="handlerMock">The handler mock.</param>
		/// <param name="uri">The uri to evaluate against.</param>
		/// <param name="httpMethod">The HTTP method the request should possess.</param>
		/// <param name="times">The times the statement should have executed.</param>
		public static void VerifyRequestWasSent(this Mock<HttpMessageHandler> handlerMock, Uri uri, HttpMethod httpMethod, Times times)
		{
			handlerMock.VerifyRequestWasSent(req => req.RequestUri == uri && req.Method == httpMethod, times);
		}

		/// <summary>
		/// Verifies the request was sent.
		/// </summary>
		/// <param name="handlerMock">The handler mock.</param>
		/// <param name="requestConditions">The request conditions as a predicate to evaluate against.</param>
		/// <param name="times">he times the statement should have executed.</param>
		public static void VerifyRequestWasSent(this Mock<HttpMessageHandler> handlerMock, Expression<Func<HttpRequestMessage, bool>> requestConditions, Times times)
		{
			handlerMock
				.Protected()
				.Verify("SendAsync", times, ItExpr.Is(requestConditions), ItExpr.IsAny<CancellationToken>());
		}
	}
}