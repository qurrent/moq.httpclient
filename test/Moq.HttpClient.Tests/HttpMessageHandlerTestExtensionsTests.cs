﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using Qurrent.Net.Http.TestExtensions;

namespace Moq.HttpClient.Tests
{
	[TestFixture]
	internal class HttpMessageHandlerTestExtensionsTests
	{
		private Mock<HttpMessageHandler> _handlerMock;
		private readonly Uri _uri = new Uri("http://0.0.0.0");
		private HttpContent _content;
		private HttpResponseMessage _response;
		private HttpMethod _method;
		private HttpStatusCode _statusCode;
		private System.Net.Http.HttpClient _client;
		private const string StringContentKey = "Whatever";

		[SetUp]
		public void SetUp()
		{
			_handlerMock = new Mock<HttpMessageHandler>();
			_content = new StringContent("SomeContent");
			_response = new HttpResponseMessage();
			_statusCode = HttpStatusCode.OK;
			_method = HttpMethod.Get;
			_client = new System.Net.Http.HttpClient(_handlerMock.Object);
		}

		[TearDown]
		public void TearDown()
		{
			_client.Dispose();
		}

		public class When_setting_up_to_return_a_response : HttpMessageHandlerTestExtensionsTests
		{
			[Test]
			public async Task It_should_evaluate_the_Uri_and_return_an_okresponse_with_content()
			{
				// Act
				_handlerMock.SetUpOkContentResponse(_uri, _content).Verifiable();
				var response = await _client.GetAsync(_uri, CancellationToken.None);

				// Assert
				response.StatusCode.Should().Be(HttpStatusCode.OK);
				response.Content.ShouldBeEquivalentTo(_content);
				_handlerMock.Verify();
			}

			[Test]
			public async Task It_should_evaluate_the_uri_and_httpmethod_and_return_an_okresponse_with_content()
			{
				// Arrange
				_method = HttpMethod.Post;

				// Act
				_handlerMock.SetUpOkContentResponse(_uri, _method, _content).Verifiable();
				var response = await _client.PostAsync(_uri, new StringContent(StringContentKey), CancellationToken.None);

				// Assert
				response.StatusCode.Should().Be(HttpStatusCode.OK);
				response.Content.ShouldBeEquivalentTo(_content);
				_handlerMock.Verify();
			}

			[Test]
			public async Task It_should_evaluate_the_predicate_and_return_an_okresponse_with_content()
			{
				// Arrange
				var returnedContent = new ByteArrayContent(new[] { byte.MinValue, Byte.MaxValue });
				var request = new HttpRequestMessage(HttpMethod.Head, _uri)
				{
					Headers =
					{
						{ "key", "value" }
					}
				};

				// Act
				_handlerMock.CustomSetUpOkContentResponse(req => req.Headers.Contains("key"), returnedContent).Verifiable();
				var response = await _client.SendAsync(request, CancellationToken.None);

				// Assert
				response.StatusCode.Should().Be(HttpStatusCode.OK);
				response.Content.ShouldBeEquivalentTo(returnedContent);
				_handlerMock.Verify();
			}

			[Test]
			public async Task It_should_be_able_to_evaluate_the_uri_and_httpmethod_and_return_a_custom_response()
			{
				// Act
				_handlerMock.SetUpResponse(_uri, _method, _response).Verifiable();
				var response = await _client.SendAsync(new HttpRequestMessage(_method, _uri), CancellationToken.None);

				// Assert
				response.ShouldBeEquivalentTo(_response);
				_handlerMock.Verify();
			}
		}

		public class When_setting_up_for_exception_throwing : HttpMessageHandlerTestExtensionsTests
		{
			private const string ErrorMessage = "SomeErrorMessage";
			private Exception _exception;

			[SetUp]
			public void SetUp()
			{
				_exception = new HttpRequestException(ErrorMessage);
			}

			[Test]
			public void It_should_be_able_to_evaluate_the_uri_and_throw_an_exception()
			{
				// Arrange
				const string errorMessage = "SomeErrorMessage";
				var exception = new HttpRequestException(errorMessage);

				// Act
				_handlerMock.SetUpThrowsException(_uri, exception).Verifiable();
				Func<Task> act = async () => await _client.GetAsync(_uri, CancellationToken.None);

				// Assert
				var ex = act.ShouldThrow<HttpRequestException>().Which;
				ex.Message.ShouldBeEquivalentTo(errorMessage);
				_handlerMock.Verify();
			}

			[Test]
			public void It_should_be_able_to_evaluate_the_uri_and_httpmethod_and_throw_an_exception()
			{
				// Arrange
				_method = HttpMethod.Post;

				// Act
				_handlerMock.SetUpThrowsException(_uri, _method, _exception).Verifiable();
				Func<Task> act = async () => await _client.PostAsync(_uri, new StringContent("whatever"), CancellationToken.None);

				// Assert
				var ex = act.ShouldThrow<HttpRequestException>().Which;
				ex.Message.ShouldBeEquivalentTo(ErrorMessage);
				_handlerMock.Verify();
			}

			[Test]
			public void It_should_be_able_to_evaluate_the_uri_and_throw_a_generic_exception()
			{
				// Act
				_handlerMock.SetUpThrowsException<InvalidCastException>(_uri).Verifiable();
				Func<Task> act = async () => await _client.GetAsync(_uri, CancellationToken.None);

				// Assert
				act.ShouldThrow<InvalidCastException>();
				_handlerMock.Verify();
			}

			[Test]
			public void It_should_be_able_to_evaluate_the_uri_and_method_and_throw_a_generic_exception()
			{
				// Arrange
				_method = HttpMethod.Post;

				// Act
				_handlerMock.SetUpThrowsException<InvalidCastException>(_uri, _method).Verifiable();
				Func<Task> act = async () => await _client.PostAsync(_uri, new StringContent("whatever"), CancellationToken.None);

				// Assert
				act.ShouldThrow<InvalidCastException>();
				_handlerMock.Verify();
			}
		}

		public class When_doing_general_setups : HttpMessageHandlerTestExtensionsTests
		{
			[Test]
			public async Task It_should_be_able_to_do_a_general_setup_and_evaluate_the_uri()
			{
				// Arrange
				var request = new HttpRequestMessage(_method, _uri);

				// Act
				_handlerMock
					.SetupSendAsync(_uri)
					.ReturnsAsync(_response)
					.Verifiable();

				var response = await _client.SendAsync(request, CancellationToken.None);

				// Assert
				response.ShouldBeEquivalentTo(_response);
				_handlerMock.Verify();
			}

			[Test]
			public async Task It_should_be_able_to_do_a_general_setup_and_evaluate_the_uri_and_httpmethod()
			{
				// Arrange
				_method = HttpMethod.Head;
				var request = new HttpRequestMessage(_method, _uri);

				// Act
				_handlerMock
					.SetupSendAsync(_uri, _method)
					.ReturnsAsync(_response)
					.Verifiable();
				var response = await _client.SendAsync(request, CancellationToken.None);

				// Assert
				response.ShouldBeEquivalentTo(_response);
				_handlerMock.Verify();
			}
		}

		public class When_verifying_whether_the_request_was_sent : HttpMessageHandlerTestExtensionsTests
		{
			[SetUp]
			public new void SetUp()
			{
				_handlerMock
					.SetupSendAsync(_uri, _method)
					.ReturnsAsync(_response);
			}

			[Test]
			public async Task It_should_verify_whether_the_uri_was_hit_5_times()
			{
				// Act
				for (var i = 0; i < 5; i++)
					await _client.GetAsync(_uri);

				// Assert
				_handlerMock.VerifyRequestWasSent(_uri, Times.Exactly(5));
			}

			[Test]
			public async Task It_should_verify_whether_the_uri_and_method_were_hit_5_times()
			{
				// Act
				for (var i = 0; i < 5; i++)
					await _client.GetAsync(_uri);

				// Assert
				_handlerMock.VerifyRequestWasSent(_uri, _method, Times.Exactly(5));
			}
		}
	}
}